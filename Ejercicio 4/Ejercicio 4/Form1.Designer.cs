﻿namespace Ejercicio_4
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.altura = new System.Windows.Forms.Label();
            this.peso = new System.Windows.Forms.Label();
            this.imc = new System.Windows.Forms.Label();
            this.texal = new System.Windows.Forms.TextBox();
            this.texpe = new System.Windows.Forms.TextBox();
            this.teximc = new System.Windows.Forms.TextBox();
            this.calcular = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // altura
            // 
            this.altura.AutoSize = true;
            this.altura.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.altura.Location = new System.Drawing.Point(20, 44);
            this.altura.Name = "altura";
            this.altura.Size = new System.Drawing.Size(51, 20);
            this.altura.TabIndex = 0;
            this.altura.Text = "Altura";
            // 
            // peso
            // 
            this.peso.AutoSize = true;
            this.peso.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.peso.Location = new System.Drawing.Point(26, 116);
            this.peso.Name = "peso";
            this.peso.Size = new System.Drawing.Size(45, 20);
            this.peso.TabIndex = 1;
            this.peso.Text = "Peso";
            // 
            // imc
            // 
            this.imc.AutoSize = true;
            this.imc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.imc.Location = new System.Drawing.Point(33, 202);
            this.imc.Name = "imc";
            this.imc.Size = new System.Drawing.Size(38, 20);
            this.imc.TabIndex = 2;
            this.imc.Text = "IMC";
            // 
            // texal
            // 
            this.texal.Location = new System.Drawing.Point(103, 44);
            this.texal.Name = "texal";
            this.texal.Size = new System.Drawing.Size(100, 20);
            this.texal.TabIndex = 3;
            // 
            // texpe
            // 
            this.texpe.Location = new System.Drawing.Point(103, 118);
            this.texpe.Name = "texpe";
            this.texpe.Size = new System.Drawing.Size(100, 20);
            this.texpe.TabIndex = 4;
            // 
            // teximc
            // 
            this.teximc.Location = new System.Drawing.Point(103, 202);
            this.teximc.Name = "teximc";
            this.teximc.Size = new System.Drawing.Size(100, 20);
            this.teximc.TabIndex = 5;
            // 
            // calcular
            // 
            this.calcular.Location = new System.Drawing.Point(89, 256);
            this.calcular.Name = "calcular";
            this.calcular.Size = new System.Drawing.Size(75, 23);
            this.calcular.TabIndex = 6;
            this.calcular.Text = "Calcular";
            this.calcular.UseVisualStyleBackColor = true;
            this.calcular.Click += new System.EventHandler(this.calcular_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(278, 323);
            this.Controls.Add(this.calcular);
            this.Controls.Add(this.teximc);
            this.Controls.Add(this.texpe);
            this.Controls.Add(this.texal);
            this.Controls.Add(this.imc);
            this.Controls.Add(this.peso);
            this.Controls.Add(this.altura);
            this.Name = "Form1";
            this.Text = "Calculadora de (IMC)";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label altura;
        private System.Windows.Forms.Label peso;
        private System.Windows.Forms.Label imc;
        private System.Windows.Forms.TextBox texal;
        private System.Windows.Forms.TextBox texpe;
        private System.Windows.Forms.TextBox teximc;
        private System.Windows.Forms.Button calcular;
    }
}

