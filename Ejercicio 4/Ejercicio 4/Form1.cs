﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio_4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calcular_Click(object sender, EventArgs e)
        {
            float primero, segundo, resultado;
            primero = float.Parse(texal.Text);
            segundo = float.Parse(texpe.Text);
            resultado = segundo / (primero * primero);
            teximc.Text = Convert.ToString(resultado);
        }
    }
}
